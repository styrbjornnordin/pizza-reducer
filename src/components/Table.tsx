import * as React from 'react';
import { connect } from "react-redux";

import pizza_slices from '../images/pizza_slices.png';

interface connectedTableProps {
    unsliced: boolean;
    remainingSlices: number;
    sogginess: string;
}

const mapStateToProps = (state: connectedTableProps) => state;

class connectedTable extends React.Component<connectedTableProps, {}> {

    public render() {
        const { unsliced, remainingSlices, sogginess } = this.props;

        const cutThePizza = (pizzaSize: boolean) => {
            return (pizzaSize ? "a-whole-pizza" : "a-sliced-pizza");
        }

        let pizzaSlices = []

        for (var i = 0; i < remainingSlices / 2; i++) {
            pizzaSlices.push(<img className={"pizza-slice slice-" + i} src={pizza_slices} key={i} />)
        }

        return (
            <div id="pizza-wrapper">
                <div id="pizza-container" className={cutThePizza(unsliced)}>
                    {pizzaSlices}
                </div>
                <div id="text-container">
                    <p>Remaining slices: {remainingSlices}</p>
                    <p>How is the pizza? - "{sogginess}"</p>
                </div>
            </div>
        );
    }
}

const Table = connect(mapStateToProps)(connectedTable);

export default Table;