import { SLICE, EAT, SOGG, REBAKE } from '../constants/action-types';

export const initialPizza = {
  unsliced: true,
  remainingSlices: 8,
  sogginess: 'Fresh'
};

const rootReducer = (state = initialPizza, action: any) => {
  switch(action.type) {
    case SLICE:
      return {...state, unsliced: false};

    case EAT:
      return {
        ...state,
        remainingSlices: state.remainingSlices - action.bites
      };

    case SOGG:
      return {...state, sogginess: "It's gettin' soggy!"};

    case REBAKE:
      return {...state, ...initialPizza};
      
    default:
      return state;
  }
};

export default rootReducer;