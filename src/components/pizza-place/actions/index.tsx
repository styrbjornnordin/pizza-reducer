import { SLICE, EAT, SOGG, REBAKE } from "../constants/action-types"; 

export const slice = { type: SLICE};

export const eat = (bites: any) => ({ type: EAT, bites: 2});

export const sogg = { type: SOGG};

export const rebake = { type: REBAKE};
