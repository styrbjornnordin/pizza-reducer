import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import './main.css';
import registerServiceWorker from './registerServiceWorker';
import Kitchen from 'src/containers/Kitchen';
import store from 'src/components/pizza-place/store';

ReactDOM.render(
  <Provider store={store}>
    <Kitchen />
  </Provider>,
  document.getElementById('app')
);
registerServiceWorker();
