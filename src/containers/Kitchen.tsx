import * as React from 'react';
import { connect } from 'react-redux';

import Table from 'src/components/Table';
import { eat, slice, sogg, rebake } from 'src/components/pizza-place/actions';
import store from 'src/components/pizza-place/store';

interface connectedKitchenProps {
    remainingSlices: number;
}

const timer = () => {
    const tenSeconds = 10000;
    setTimeout(() => {
        store.dispatch(sogg);
    }, tenSeconds);
}

const mapStateToProps = (state: connectedKitchenProps) => state;

const eatThePizza = () => {
    store.dispatch( eat({ bites: 2 }) );
    store.dispatch(slice);
}

let restoreThePizza = () => {
    store.dispatch(rebake);
    timer();
}

timer();

class connectedKitchen extends React.Component<connectedKitchenProps, {}> {    
    render() {
        const {remainingSlices} = this.props;

        let eatThePizzaButton = <div id="eat-pizza-button" onClick={eatThePizza}>Eat two slices!</div>;

        if(remainingSlices <= 0) {
            eatThePizzaButton = <div id="eat-pizza-button">No more pizza!</div>;
        }

        return (
            <div id="kitchen">
                <h2>Welcome to the Pizza Restaurant!</h2>
                <Table />
                <div id="button-wrapper">
                    {eatThePizzaButton}
                    <div id="bake-pizza-button" onClick={restoreThePizza}>Bake a new pizza!</div>
                </div>
            </div>
        );
    }
}

const Kitchen = connect(mapStateToProps)(connectedKitchen);

export default Kitchen;